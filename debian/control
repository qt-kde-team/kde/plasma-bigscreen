Source: plasma-bigscreen
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Scarlett Moore <sgmoore@debian.org>,
           Patrick Franz <deltaone@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules,
               kirigami2-dev (>= 5.102.0~),
               libkf5activities-dev (>= 5.102.0~),
               libkf5activitiesstats-dev (>= 5.102.0~),
               libkf5declarative-dev (>= 5.102.0~),
               libkf5i18n-dev (>= 5.102.0~),
               libkf5kcmutils-dev (>= 5.102.0~),
               libkf5kio-dev (>= 5.102.0~),
               libkf5notifications-dev (>= 5.102.0~),
               libkf5plasma-dev (>= 5.102.0~),
               libkf5wayland-dev (>= 4:5.102.0~),
               libkf5windowsystem-dev (>= 5.102.0~),
               pkg-kde-tools (>= 0.15.16),
               plasma-framework,
               plasma-workspace-dev (>= 4:5.27.11~),
               qtbase5-dev (>= 5.15.2~),
               qtdeclarative5-dev (>= 5.15.2~),
               qtmultimedia5-dev (>= 5.15.2~),
Standards-Version: 4.7.0
Homepage: https://invent.kde.org/plasma/plasma-bigscreen
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/plasma-bigscreen
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/plasma-bigscreen.git
Rules-Requires-Root: no

Package: plasma-bigscreen
Architecture: any
Depends: plasma-framework,
         plasma-nano,
         plasma-nm,
         plasma-workspace,
         python3,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Plasma Bigscreen for TVs
 A big launcher giving you easy access to any installed apps and skills.
 Controllable via voice or TV remote.
 This project is using various open-source components like Plasma Bigscreen,
 Mycroft AI and libcec.
 .
 Voice Control
 Bigscreen supports Mycroft AI, a free and open-source voice assistant that
 can be run completely decentralized on your own server.
 .
 Remote control your TV via CEC
 Consumer Electronics Control is a standard to control devices over HDMI.
 Use your normal TV remote control, or a RC with built-in microphone for
 voice control and optional mouse simulation.
 .
 Coming soon:
 Voice apps
 Download new apps (aka skills) for your Bigscreen or add your own ones
 for others to enjoy.
